<?php
return [
    'main' => [
        'user' => [
            0 => ['John', 'Doe', 'john.doe@mail.com', crypt('JohnDoe', md5('john.doe@mail.com'))]
        ],
        'acl' => [
            // ['main_user_id', 'main_model_id', 'write', 'read']
        ],
        'model' => [
            ['main.user',   App\Controller\User::class],
            ['main.acl',    App\Controller\Acl::class],
            ['main.model',  App\Controller\Model::class],
        ]
    ]
];
