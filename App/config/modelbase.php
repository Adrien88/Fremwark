<?php
return [
    'main' => [

        // 'user' => [
        //     ['id',          'INTEGER',  16,     'nullable' => true,     'options' => ['AUTO_INCREMENT', 'PRIMARY_KEY']],
        //     ['email',       'VARCHAR',  255,    'nullable' => false,      'index' => ['UNIQUE', 'email'],],
        //     ['lastname',    'VARCHAR',  50],
        //     ['firstname',   'VARCHAR',  50],
        // ],

        // 'user_auth' => [
        //     ['user_id',     'INTEGER',   16,     'nullable' => false,   'index' => ['user_id',  'delete' => true, 'update' => true]],
        //     ['password',    'VARCHAR',  255,     'nullable' => false,   'index' => null],
        //     ['token',       'VARCHAR',  255,     'nullable' => true,    'index' => null],
        // ],

        // 'user_acl' => [
        //     ['user_id',     'INTEGER',   16,     'nullable' => false,   'index' => ['user_id',  'delete' => true, 'update' => true]],
        //     ['acl_id',      'INTEGER',   16,     'nullable' => false,   'index' => ['acl_id',   'delete' => true, 'update' => true]],
        // ],

        'acl' => [
            ['id',          'INTEGER',  16,     'nullable' => true,     'options' => ['AUTO_INCREMENT', 'PRIMARY_KEY']],
            ['parent',      'INTEGER',  16,     'nullable' => false,    'index' => ['user_acl',  'delete' => true, 'update' => true],],
            ['owner',       'INTEGER',  16,     'nullable' => false,    'index' => ['user_id',  'delete' => true, 'update' => true]],
            ['name',        'VARCHAR',  50],
            ['read',        'TINYINT',  1, 'options' => ['default' => 0]],
            ['write',       'TINYINT',  1, 'options' => ['decrcreateeatefault' => 0]],
            ['create',      'TINYINT',  1, 'options' => ['default' => 0]],
            ['delete',      'TINYINT',  1, 'options' => ['default' => 0]],
        ],

        'model' => [
            ['id',          'INTEGER',  16,     'nullable' => true,     'options' => ['AUTO_INCREMENT', 'PRIMARY_KEY']],
            ['name',        'VARCHAR',  255],
        ],

        'model_acl' => [
            ['model_id',    'INTEGER',   16,     'nullable' => false,   'index' => ['model_id',  'delete' => true, 'update' => true]],
            ['acl_id',      'INTEGER',   16,     'nullable' => false,   'index' => ['acl_id',   'delete' => true, 'update' => true]],
        ]
    ]
];
